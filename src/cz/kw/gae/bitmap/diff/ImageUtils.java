package cz.kw.gae.bitmap.diff;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;

import ar.com.hjg.pngj.ImageLine;
import ar.com.hjg.pngj.PngReader;
import ar.com.hjg.pngj.PngWriter;

// gae neumoznuje pristupovat k pixelum... muzeme pouzit jen https://github.com/jakeri/pngj-for-Google-App-Engine
// nova verze pngj uz defaultne funguje pod AppEngine https://code.google.com/p/pngj/

public class ImageUtils {
	/**
	 * Zjisti a vrati png s rozdily.
	 */
	public static byte[] getDiff(byte[] changedImageData, byte[] originalImageData) {
		return internalDiff(changedImageData, originalImageData);
	}

	/**
	 * Aplikuje zmeny na obrazek a vrati novy pozmeneny obrazek.
	 */
	public static byte[] applyDiff(byte[] imageData, byte[] diffData) {
		return internalDiff(imageData, diffData);
	}

	/**
	 * 
	 * @param imageData1
	 *            - Diff je vypocten jako prechod od imageData1 k imageData2
	 * @param imageData2
	 *            - Obrazek vuci kteremu se Diff pocita. Tento obrazek je pote zdrojem pro undiff
	 */
	private static byte[] internalDiff(byte[] imageData1, byte[] imageData2) {
		// vytvoreni PNG z obrdzenych dat:
		PngReader png1 = new PngReader(new ByteArrayInputStream(imageData1), "loool.png");
		PngReader png2 = new PngReader(new ByteArrayInputStream(imageData2), "loool2.png");

		// vytvoreni vystupniho PNG
		ByteArrayOutputStream outputByteStream = new ByteArrayOutputStream();
		PngWriter pngWriter = new PngWriter(outputByteStream, png1.imgInfo);

		int rows = png1.imgInfo.rows;
		for (int row = 0; row < rows; row++) { // pres vsechny radky
			ImageLine line1 = png1.readRow(row);
			ImageLine line2;
			if (row < png2.imgInfo.rows) { // zarovnani radku, pokud by chybeli, prebyvajici neni traba resit
				line2 = png2.readRow(row);
			} else {
				line2 = new ImageLine(line1.imgInfo);
			}

			int channels1 = line1.imgInfo.channels;
			int columns1 = line1.imgInfo.cols;

			int channels2 = line2.imgInfo.channels;
			int columns2 = line2.imgInfo.cols;

			int[] computedDiffLine = line1.scanline.clone();
			for (int column = 0; column < columns1; column++) { // pres vsechny sloupce
				for (int channel = 0; channel < channels1; channel++) { // vsechny barevne kanaly
					int index1 = column * channels1 + channel;
					int channel1 = line1.scanline[index1];
					int channel2;
					if (column < columns2) { // zarovnani barevnych kanalu, pokud by chybeli, prebyvajici netreba resit protoze PNG je RGBA
						if (channel < channels2) {
							int index2 = column * channels2 + channel;
							channel2 = line2.scanline[index2];
						} else {
							channel2 = 0;
						}
					} else {
						channel2 = 0;
					}

					computedDiffLine[index1] = channel1 ^ channel2; // provedeni XOR nad barevnym kanalem daneho pixelu
				}
			}

			pngWriter.writeRow(computedDiffLine); // Ulozeni radku do vysledku

		}
		pngWriter.end();

		return outputByteStream.toByteArray(); // jako bytove pole
	}
}
