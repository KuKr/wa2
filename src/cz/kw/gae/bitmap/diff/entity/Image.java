package cz.kw.gae.bitmap.diff.entity;

import javax.jdo.annotations.IdGeneratorStrategy;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;

import com.google.appengine.api.datastore.Key;

@PersistenceCapable
public class Image {
	@PrimaryKey
	@Persistent(valueStrategy = IdGeneratorStrategy.IDENTITY)
	private Key imageKey;

	@Persistent
	private String userId;

	@Persistent
	private String name;
	
	@Persistent
	private Key actualDiff;

	public Image() {
		// non-parametric constructor for jdo
	}

	public Image(String userId, String name) {
		this.userId = userId;
		this.name = name;
	}

	public Key getImageKey() {
		return imageKey;
	}

	public void setImageKey(Key imageKey) {
		this.imageKey = imageKey;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Key getActualDiff() {
		return actualDiff;
	}

	public void setActualDiff(Key actualDiff) {
		this.actualDiff = actualDiff;
	}

}
