package cz.kw.gae.bitmap.diff.entity;

import javax.jdo.annotations.IdGeneratorStrategy;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;

import com.google.appengine.api.datastore.Blob;
import com.google.appengine.api.datastore.Key;

@PersistenceCapable
public class Diff implements Comparable<Diff> {
	@PrimaryKey
	@Persistent(valueStrategy = IdGeneratorStrategy.IDENTITY)
	private Key diffKey;

	@Persistent
	private Key image;

	@Persistent
	private Key parent;

	@Persistent
	private long timestamp;

	@Persistent
	private Blob data;

	public Diff() {
		// non-parametric constructor for jdo
	}

	public Diff(Key image, Key parrent, Blob data) {
		this.image = image;
		this.parent = parrent;
		this.timestamp = System.currentTimeMillis();
		this.setData(data);
	}

	public Key getDiffKey() {
		return diffKey;
	}

	public void setDiffKey(Key diffKey) {
		this.diffKey = diffKey;
	}

	public Key getImage() {
		return image;
	}

	public void setImage(Key image) {
		this.image = image;
	}

	public Key getParent() {
		return parent;
	}

	public void setParent(Key parent) {
		this.parent = parent;
	}

	public long getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(long timestamp) {
		this.timestamp = timestamp;
	}

	public Blob getData() {
		return data;
	}

	public void setData(Blob data) {
		this.data = data;
	}

	@Override
	public int compareTo(Diff o) {
		return (int) (o.timestamp - this.timestamp);
	}
}
