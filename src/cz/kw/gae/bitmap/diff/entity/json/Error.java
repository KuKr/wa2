package cz.kw.gae.bitmap.diff.entity.json;

import java.io.IOException;

import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;


public class Error {
	private Integer code;
	private String message;

	public Error(int code, String message) {
		this.code = code;
		this.message = message;
	}

	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	
	public static void defaultError(Gson gson,Exception ex, HttpServletResponse resp) throws IOException {
		resp.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
		resp.getWriter().print(gson.toJson(new Error(-40000, "unexpected error: " + ex.toString())));
	}
	
	public static void unauthorizedError(Gson gson, HttpServletResponse resp) throws IOException {
		resp.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
		resp.getWriter().print(gson.toJson(new Error(-40001, "Unauthenticated user")));
	}

}
