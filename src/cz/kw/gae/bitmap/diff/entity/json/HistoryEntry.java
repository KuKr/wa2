package cz.kw.gae.bitmap.diff.entity.json;

import com.google.appengine.api.datastore.KeyFactory;

import cz.kw.gae.bitmap.diff.entity.Diff;

public class HistoryEntry {
	private String key;
	private Long timestamp;
	private Long size;
	
	public HistoryEntry(Diff diff) {
		this.key = KeyFactory.keyToString(diff.getDiffKey());
		this.timestamp = diff.getTimestamp();
		this.size = (long) diff.getData().getBytes().length;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public Long getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Long timestamp) {
		this.timestamp = timestamp;
	}

	public Long getSize() {
		return size;
	}

	public void setSize(Long size) {
		this.size = size;
	}
}
