package cz.kw.gae.bitmap.diff.entity.json;

import com.google.appengine.api.datastore.KeyFactory;

import cz.kw.gae.bitmap.diff.entity.Image;

public class ImageEntry {
	private String key;
	private String name;
	private String historyTopKey;

	public ImageEntry(Image image) {
		this.key = KeyFactory.keyToString(image.getImageKey());
		this.name = image.getName();
		this.historyTopKey = KeyFactory.keyToString(image.getActualDiff());
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getHistoryTopKey() {
		return historyTopKey;
	}

	public void setHistoryTopKey(String historyTopKey) {
		this.historyTopKey = historyTopKey;
	}
}
