package cz.kw.gae.bitmap.diff;

import javax.jdo.JDOHelper;
import javax.jdo.PersistenceManagerFactory;

public final class PMF {
    private static final PersistenceManagerFactory pmfInstance = JDOHelper.getPersistenceManagerFactory("transactions-optional");

    private PMF() {}

    /**
     * @return Persistence manager factory
     */
    public static PersistenceManagerFactory get() {
        return pmfInstance;
    }
}