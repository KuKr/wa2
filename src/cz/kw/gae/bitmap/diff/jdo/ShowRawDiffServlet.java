package cz.kw.gae.bitmap.diff.jdo;

import java.util.List;

import javax.jdo.Query;

import com.google.appengine.api.datastore.Key;

import cz.kw.gae.bitmap.diff.entity.Diff;

@SuppressWarnings("serial")
public class ShowRawDiffServlet extends ShowImageServlet {

	@Override
	protected byte[] getImage(Query diffQuery, Key diffKey) {
		@SuppressWarnings("unchecked")
		List<Diff> diffs = (List<Diff>) diffQuery.execute(diffKey); // list history item
		if (diffs == null || diffs.isEmpty()) { // is any?
			return null;
		} else {
			Diff diff = diffs.get(0);
			return diff.getData().getBytes(); // return raw diff data
		}
	}
}
