package cz.kw.gae.bitmap.diff.jdo;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItemIterator;
import org.apache.commons.fileupload.FileItemStream;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.io.IOUtils;

import com.google.appengine.api.datastore.Blob;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.users.User;
import com.google.appengine.api.users.UserService;
import com.google.appengine.api.users.UserServiceFactory;
import com.google.gson.Gson;

import cz.kw.gae.bitmap.diff.ImageUtils;
import cz.kw.gae.bitmap.diff.PMF;
import cz.kw.gae.bitmap.diff.entity.Diff;
import cz.kw.gae.bitmap.diff.entity.Image;
import cz.kw.gae.bitmap.diff.entity.json.Error;
import cz.kw.gae.bitmap.diff.entity.json.HistoryEntry;

@SuppressWarnings("serial")
public class AddVersionServlet extends HttpServlet {

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		Gson gson = new Gson();
		
		try {
			UserService userService = UserServiceFactory.getUserService();
			User currentUser = userService.getCurrentUser();

			if (currentUser != null && currentUser.getUserId() != null) { // check user
				ServletFileUpload fileUpload = new ServletFileUpload();
				FileItemIterator fileIterator = fileUpload.getItemIterator(req);
				String imageKey = null;
				byte[] newImage = null;
				
				while (fileIterator.hasNext()) { // iterate over form fields
					FileItemStream fileStream = fileIterator.next();
					InputStream stream = fileStream.openStream();
					
					if (fileStream.isFormField()) { // process all form fields
						if (fileStream.getFieldName().equals("image")) { // image key
							byte[] imageKeyData = IOUtils.toByteArray(stream);
							imageKey = new String(imageKeyData);
						}
					} else if (fileStream.getContentType().startsWith("image/")) { // process all images
						newImage = IOUtils.toByteArray(stream);
					} else { // unsupported media type (non image file)
						resp.setStatus(HttpServletResponse.SC_UNSUPPORTED_MEDIA_TYPE);
						resp.getWriter().print(gson.toJson(new Error(-40004, "Only images are allowed here")));
						return;
					}
				}

				if (imageKey == null || imageKey.isEmpty()) { // is image key empty?
					resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
					resp.getWriter().print(gson.toJson(new Error(-40005, "image key is missing")));
					return;
				} else {
					PersistenceManager persistenceMgr = PMF.get().getPersistenceManager();
	
					Query imageQuery = persistenceMgr.newQuery(Image.class);
					imageQuery.setFilter("imageKey == keyParam");
					imageQuery.declareParameters(Image.class.getSimpleName() + " keyParam");
					
					try {
						@SuppressWarnings("unchecked")
						List<Image> images = (List<Image>) imageQuery.execute(KeyFactory.stringToKey(imageKey)); // get image by image key
						if (images == null || images.isEmpty()) { // is image exists?
							resp.setStatus(HttpServletResponse.SC_NOT_FOUND);
							resp.getWriter().print(gson.toJson(new Error(-40010, "image not found")));
							return;
						} else {
							Query diffQuery = persistenceMgr.newQuery(Diff.class);
							diffQuery.setFilter("diffKey == keyParam");
							diffQuery.declareParameters(Diff.class.getSimpleName() + " keyParam");
							@SuppressWarnings("unchecked")
							List<Diff> diffs = (List<Diff>) diffQuery.execute(images.get(0).getActualDiff()); // get root history item
							if (diffs == null || diffs.isEmpty()) { // is root diff item
								resp.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
								resp.getWriter().print(gson.toJson(new Error(-40010, "cannot create diff")));
								return;
							} else {
								Diff diff = new Diff(images.get(0).getImageKey(), null, new Blob(newImage)); // create and persist new Diff object
								persistenceMgr.makePersistent(diff);

								Blob oldBlob = diffs.get(0).getData();
								byte[] oldImage = oldBlob.getBytes();
								
								byte[] diffImage = ImageUtils.getDiff(oldImage, newImage);							
								
								diffs.get(0).setData(new Blob(diffImage));
								
								diffs.get(0).setParent(diff.getDiffKey()); // update old image diff
								persistenceMgr.makePersistent(diffs.get(0));
								
								images.get(0).setActualDiff(diff.getDiffKey()); // set actual diff
								persistenceMgr.makePersistent(images.get(0));
								
								HistoryEntry entry = new HistoryEntry(diff);
								resp.setStatus(HttpServletResponse.SC_CREATED);
								resp.getWriter().print(gson.toJson(entry));
								return;
							}
						}
					} finally {
						persistenceMgr.close();
					}
				}
			} else {
				Error.unauthorizedError(gson, resp);
				return;
			}
		} catch (Exception ex) {
			Error.defaultError(gson, ex, resp);
			return;
		}
	}
	
	// XXX just for debug
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		resp.getOutputStream().println("<html><head></head><body>" +
				"<form action=\"update\" method=\"post\" enctype=\"multipart/form-data\"/>" +
				"<input name=\"imageField\" type=\"file\" size=\"30\"/> <br/>" +
				"<input name=\"image\" type=\"text\" size=\"100\"/> <br/>" +
				"<input name=\"Submit\" type=\"submit\" value=\"Sumbit\">" +
				"</form></body>");
	}
}
