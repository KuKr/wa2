package cz.kw.gae.bitmap.diff.jdo;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItemIterator;
import org.apache.commons.fileupload.FileItemStream;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.io.IOUtils;

import com.google.appengine.api.datastore.Blob;
import com.google.appengine.api.users.User;
import com.google.appengine.api.users.UserService;
import com.google.appengine.api.users.UserServiceFactory;
import com.google.gson.Gson;

import cz.kw.gae.bitmap.diff.PMF;
import cz.kw.gae.bitmap.diff.entity.Diff;
import cz.kw.gae.bitmap.diff.entity.Image;
import cz.kw.gae.bitmap.diff.entity.json.Error;
import cz.kw.gae.bitmap.diff.entity.json.ImageEntry;

@SuppressWarnings("serial")
public class CreateImageServlet extends HttpServlet {
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		Gson gson = new Gson();
		
		try {
			UserService userService = UserServiceFactory.getUserService();
			User currentUser = userService.getCurrentUser();

			if (currentUser != null && currentUser.getUserId() != null) { // check user 
				ServletFileUpload fileUpload = new ServletFileUpload();
				FileItemIterator fileIterator = fileUpload.getItemIterator(req);

				while (fileIterator.hasNext()) { // iterate over form fields
					FileItemStream fileStream = fileIterator.next();
					InputStream stream = fileStream.openStream();

					if (fileStream.isFormField()) { // process all form fields
						System.out.println(fileStream.getFieldName());
					} else if (fileStream.getContentType().startsWith("image/")) { // process all images
						String imageName = fileStream.getName(); // image original name
						if (imageName != null && !imageName.isEmpty()) {
							PersistenceManager persistenceMgr = PMF.get().getPersistenceManager();
							
							Query imageQuery = persistenceMgr.newQuery(Image.class);
							imageQuery.setFilter("name == nameParam");
							imageQuery.declareParameters(Image.class.getSimpleName() + " nameParam");
							
							try {
								@SuppressWarnings("unchecked")
								List<Image> images = (List<Image>) imageQuery.execute(imageName);
								if (images == null || images.isEmpty()) { // check if image exist
									Image image = new Image(currentUser.getUserId(), imageName); // create and persist Image object
									persistenceMgr.makePersistent(image);
									Diff diff = new Diff(image.getImageKey(), null, new Blob(IOUtils.toByteArray(stream))); // create and persist first image's Diff object
									persistenceMgr.makePersistent(diff);
									image.setActualDiff(diff.getDiffKey()); // set actual diff
									persistenceMgr.makePersistent(image);
									
									ImageEntry entry = new ImageEntry(image);
									resp.setStatus(HttpServletResponse.SC_CREATED);
									resp.getWriter().print(gson.toJson(entry));
									return;
								} else { // image conflict
									resp.setStatus(HttpServletResponse.SC_CONFLICT);
									resp.getWriter().print(gson.toJson(new Error(-40002, "image with name \"" + imageName + "\" allready exist")));
									return;
								}
							} finally {
								persistenceMgr.close();
							}
						} else { // image name is emtpy
							resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
							resp.getWriter().print(gson.toJson(new Error(-40003, "Empty image name")));
							return;
						}
					} else { // unsupported media type (it is not image)
						resp.setStatus(HttpServletResponse.SC_UNSUPPORTED_MEDIA_TYPE);
						resp.getWriter().print(gson.toJson(new Error(-40004, "Only images are allowed here")));
						return;
					}
				}
			} else {
				Error.unauthorizedError(gson, resp);
				return;
			}
		} catch (Exception ex) {
			Error.defaultError(gson, ex, resp);
			return;
		}
	}

	// XXX just for debug
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		resp.getOutputStream().println("<html><head></head><body>" +
				"<form action=\"add\" method=\"post\" enctype=\"multipart/form-data\">" +
				"<input name=\"imageField\" type=\"file\" size=\"30\"> <br/>" +
				"<input name=\"Submit\" type=\"submit\" value=\"Sumbit\">" +
				"</form></body>");
	}
}
