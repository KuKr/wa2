package cz.kw.gae.bitmap.diff.jdo;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.appengine.api.users.User;
import com.google.appengine.api.users.UserService;
import com.google.appengine.api.users.UserServiceFactory;
import com.google.gson.Gson;

import cz.kw.gae.bitmap.diff.PMF;
import cz.kw.gae.bitmap.diff.entity.Image;
import cz.kw.gae.bitmap.diff.entity.json.Error;
import cz.kw.gae.bitmap.diff.entity.json.ImageEntry;

@SuppressWarnings("serial")
public class ListImagesServlet extends HttpServlet {
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		Gson gson = new Gson();
		
		try {
			UserService userService = UserServiceFactory.getUserService();
			User currentUser = userService.getCurrentUser();
	
			if (currentUser != null && currentUser.getUserId() != null) { // check user
				PersistenceManager persistenceMgr = PMF.get().getPersistenceManager();
				Query imagesQuery = persistenceMgr.newQuery(Image.class);
				imagesQuery.setFilter("userId == userIdParam");
				imagesQuery.declareParameters(Image.class.getSimpleName() + " userIdParam");
				try {
					@SuppressWarnings("unchecked")
					List<Image> images = (List<Image>) imagesQuery.execute(currentUser.getUserId()); // list user's images
					if (images == null || images.isEmpty()) { // is any?
						resp.setStatus(HttpServletResponse.SC_NOT_FOUND);
						resp.getWriter().print(gson.toJson(new Error(-40007, "storage is empty")));
						return;
					} else {
						ArrayList<ImageEntry> entries = new ArrayList<ImageEntry>();
						for (Image image : images) { // convert to json entity
							entries.add(new ImageEntry(image));
						}
						
						resp.setStatus(HttpServletResponse.SC_OK);
						resp.getWriter().print(gson.toJson(entries));
						return;
					}
				} finally {
					persistenceMgr.close();
				}
			}  else {
				Error.unauthorizedError(gson, resp);
				return;
			}
		} catch (Exception ex) {
			Error.defaultError(gson, ex, resp);
			return;
		}
	}
}
