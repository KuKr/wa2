package cz.kw.gae.bitmap.diff.jdo;

import java.io.IOException;
import java.util.List;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.users.User;
import com.google.appengine.api.users.UserService;
import com.google.appengine.api.users.UserServiceFactory;
import com.google.gson.Gson;

import cz.kw.gae.bitmap.diff.ImageUtils;
import cz.kw.gae.bitmap.diff.PMF;
import cz.kw.gae.bitmap.diff.entity.Diff;
import cz.kw.gae.bitmap.diff.entity.json.Error;

@SuppressWarnings("serial")
public class ShowImageServlet extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		Gson gson = new Gson();
		
		try {
			UserService userService = UserServiceFactory.getUserService();
			User currentUser = userService.getCurrentUser();
	
			if (currentUser != null && currentUser.getUserId() != null) { // check user
				String diffKey = req.getParameter("history"); // retrieve history key
				
				if (diffKey == null || diffKey.isEmpty()) { // check history key
					resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
					resp.getWriter().print(gson.toJson(new Error(-40008, "history key is missing")));
					return;
				} else {
					PersistenceManager persistenceMgr = PMF.get().getPersistenceManager();
					
					Query diffQuery = persistenceMgr.newQuery(Diff.class);
					diffQuery.setFilter("diffKey == keyParam");
					diffQuery.declareParameters(Diff.class.getSimpleName() + " keyParam");
					
					try {
						byte[] image = getImage(diffQuery, KeyFactory.stringToKey(diffKey)); // get image data
						if (image == null || image.length == 0) {// is any?
							resp.setStatus(HttpServletResponse.SC_NOT_FOUND);
							resp.getWriter().print(gson.toJson(new Error(-40009, "history cannot be shown")));
							return;
						} else {
							resp.setStatus(HttpServletResponse.SC_OK); // return image data
							resp.getOutputStream().write(image);
							return;
						}
					} finally {
						persistenceMgr.close();
					}
				}
				
			} else {
				Error.unauthorizedError(gson, resp);
				return;
			}
		} catch (Exception ex) {
			Error.defaultError(gson, ex, resp);
			return;
		}
	}
	
	/**
	 * Recursively gets image data.
	 * Makes undiff and gets original image data.
	 * 
	 * @param diffQuery - Query object for querying diffs by key
	 * @param diffKey - current diff key
	 * @return image data as byte array
	 */
	protected byte[] getImage(Query diffQuery, Key diffKey) {
		@SuppressWarnings("unchecked")
		List<Diff> diffs = (List<Diff>) diffQuery.execute(diffKey); // list history item
		if (diffs == null || diffs.isEmpty()) { // is any?
			return null;
		} else {
			Diff diff = diffs.get(0);
			if (diff.getParent() != null) { // has parent?
				byte[] data = getImage(diffQuery, diff.getParent()); // get parent data
				byte[] diffData = diff.getData().getBytes(); // get diff data
				byte[] applyDiff = ImageUtils.applyDiff(diffData, data); // apply undiff
				return applyDiff;
			} else {
				return diff.getData().getBytes(); // has no parent, return whole data
			}
		}
	}
}
