package cz.kw.gae.bitmap.diff.jdo;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.users.User;
import com.google.appengine.api.users.UserService;
import com.google.appengine.api.users.UserServiceFactory;
import com.google.gson.Gson;

import cz.kw.gae.bitmap.diff.PMF;
import cz.kw.gae.bitmap.diff.entity.Diff;
import cz.kw.gae.bitmap.diff.entity.json.Error;
import cz.kw.gae.bitmap.diff.entity.json.HistoryEntry;

@SuppressWarnings("serial")
public class ListImageHistoryServlet extends HttpServlet {
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		Gson gson = new Gson();
		
		try {
			UserService userService = UserServiceFactory.getUserService();
			User currentUser = userService.getCurrentUser();
	
			if (currentUser != null && currentUser.getUserId() != null) { // check user
				String imageKey = req.getParameter("image"); // retrieve image key
				
				if (imageKey == null || imageKey.isEmpty()) { // is image key
					resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
					resp.getWriter().print(gson.toJson(new Error(-40005, "image key is missing")));
					return;
				} else {
					PersistenceManager persistenceMgr = PMF.get().getPersistenceManager();
	
					Query diffQuery = persistenceMgr.newQuery(Diff.class);
					diffQuery.setFilter("image == imageParam");
					diffQuery.setOrdering("timestamp desc");
					diffQuery.declareParameters(Diff.class.getSimpleName() + " imageParam");
						
					try {
						@SuppressWarnings("unchecked")
						List<Diff> diffs = (List<Diff>) diffQuery.execute(KeyFactory.stringToKey(imageKey)); // list history items for given image
						if (diffs == null || diffs.isEmpty()) { // is any?
							resp.setStatus(HttpServletResponse.SC_NOT_FOUND);
							resp.getWriter().print("image not found");
							return;
						} else {
							ArrayList<HistoryEntry> history = new ArrayList<HistoryEntry>();
							for (Diff diff : diffs) { // convert to json entity
								history.add(new HistoryEntry(diff));
							}
							
							resp.setStatus(HttpServletResponse.SC_OK);
							resp.getWriter().print(gson.toJson(history));
							return;
						}
					} finally {
						persistenceMgr.close();
					}
				}
			}  else {
				Error.unauthorizedError(gson, resp);
				return;
			}
		} catch (Exception ex) {
			Error.defaultError(gson, ex, resp);
			return;
		}
	}
}
